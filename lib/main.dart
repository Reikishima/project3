// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../Option/option.dart';
import '../Statistik/statistik.dart';
import '../Tes/tes.dart';
import '../Utils/appcolors.dart';
import '../home/tennisapp.dart';

void main() {
  runApp(const MyProject());
}
class MyProject extends StatefulWidget {
  const MyProject({super.key});

  @override
  State<MyProject> createState() => _MyProjectState();
}

class _MyProjectState extends State<MyProject> {
  //initiate variable for move state on navbar
  int _selectedIndex = 0;
  static List<Widget> navbar = <Widget>[
     const TennisApp(),
    const Statistik(),
     const Test(),
     const Option()
  ];
  //use to move state on navbar
  void _ontappedIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: AppColors.whitehome,
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: AppColors.whitehome,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home_outlined,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.location_on_outlined, 
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.description_outlined,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
              ),
              label: '',
            ),
          ],unselectedItemColor: AppColors.grey,
          selectedItemColor: AppColors.bluehome,
          currentIndex: _selectedIndex,
          onTap: _ontappedIndex,
        ),
        body: Center(
          child: navbar.elementAt(_selectedIndex),
        ),
      ),
      builder: (BuildContext context, widget) => ResponsiveWrapper.builder(
        ClampingScrollWrapper.builder(context, widget!),
        minWidth: 375,
        maxWidth: 812,
        breakpoints: const <ResponsiveBreakpoint>[
          ResponsiveBreakpoint.autoScale(375, name: MOBILE),
        ],
      ),
    );
  }
}


