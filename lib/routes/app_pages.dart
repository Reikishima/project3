// ignore_for_file: constant_identifier_names

import 'package:get/get.dart';
import 'package:project3/home/tennisapp.dart';

import '../home/tennis_binding.dart';
import '../splash/splash_binding.dart';
import '../splash/splash_view.dart';
part 'app_routes.dart';


class AppPages{
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(name: _Paths.HOME, page: ()=> const TennisApp(),binding: TennisBinding()),
    GetPage(name: _Paths.SPLASH, page: ()=> const SplashView(),binding: SplashBinding()),
    // GetPage(name: _Paths.LOGIN, page: ()=> const LoginView(),binding: LoginBinding()),
    //GetPage(name: _Paths.REGISTER, page: ()=> const RegisterView(),)
  ];
}