import 'package:flutter/material.dart';

class AppColors {
  Color _hex({required String colorCode}) {
    final String containHex = colorCode.toUpperCase().replaceAll('#', '');
    String result = '';
    if (colorCode.length == 7) {
      result = 'FF$containHex';
    }

    return Color(int.parse(result, radix: 16));
  }
  //list untuk hex color(Custom Color)
  static Color blackfirst = AppColors()._hex(colorCode: '#171415');
  static Color greenhome = AppColors()._hex(colorCode: '#016A40');
  static Color whitehome = AppColors()._hex(colorCode: '#FFFEFF');
  static Color bluehome = AppColors()._hex(colorCode: '#35008e');
  static Color grey = AppColors()._hex(colorCode: '#808080');
  static Color buttonblue= AppColors()._hex(colorCode: '#1870d5');
  static Color buttonred= AppColors()._hex(colorCode: '#FF0800');
  static Color purple= AppColors()._hex(colorCode: '#4c00b0');
  static Color purplebutton= AppColors()._hex(colorCode: '#CEC2EB');
  static Color orangebutton= AppColors()._hex(colorCode: '#FFA726');
  static Color greenbutton= AppColors()._hex(colorCode: '#1fd655');
  static Color bluebutton= AppColors()._hex(colorCode: '#0066ff');


  
  
  
  }