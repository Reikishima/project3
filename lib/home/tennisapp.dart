// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project3/Utils/appcolors.dart';
import 'package:project3/Utils/sizeconfig.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';


class TennisApp extends StatelessWidget {
  const TennisApp({super.key});
  void main() => runApp(const GetMaterialApp(home: TennisApp()));

  @override
  Widget build(BuildContext context) {
    //test git
    //test again
    SizeConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: SizeConfig.screenWidth,
              height: SizeConfig.vertical(42),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40)),
                  color: AppColors.bluehome),
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: row(context),
                  )
                ],
              ),
            ),
            Container(
              height: SizeConfig.vertical(55),
              width: SizeConfig.screenWidth,
              color: AppColors.whitehome,
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: bottom(context),
                  ),
                  // ResponsiveRowColumnItem(
                  //   child: test(context),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// Widget untuk Mengisi value yang ada di Container
Widget row(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      top: SizeConfig.vertical(2.5),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.drag_handle_rounded,
                color: AppColors.whitehome,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.notifications_none,
                color: AppColors.whitehome,
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(4),
              right: SizeConfig.horizontal(4),
              top: SizeConfig.vertical(1)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Covid-19',
                style: TextStyle(
                  color: AppColors.whitehome,
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: AppColors.whitehome),
                width: SizeConfig.horizontal(30),
                height: SizeConfig.vertical(5),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/flag-modified.png',
                      width: 55,
                      height: 20,
                    ),
                    Text(
                      'IDN',
                      style: TextStyle(
                          color: AppColors.blackfirst,
                          fontWeight: FontWeight.w500),
                    ),
                    Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.blackfirst,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(4),
              right: SizeConfig.horizontal(4),
              top: SizeConfig.vertical(1.5)),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Are you feeling sick?',
                    style: TextStyle(
                        color: AppColors.whitehome,
                        fontSize: 20,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: SizeConfig.vertical(1.5)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: RichText(
                        text: TextSpan(
                          text:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae turpis hendrerit, hendrerit ipsum vitae, imperdiet dui. Nulla accumsan urna id lacus posuere egestas. Nunc eleifend nulla vel diam faucibus placerat a mattis sem.',
                          style: TextStyle(
                              color: AppColors.grey,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(4),
              right: SizeConfig.horizontal(4),
              top: SizeConfig.vertical(1.5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: SizeConfig.vertical(6),
                width: SizeConfig.horizontal(44),
                child: TextButton.icon(
                  style: TextButton.styleFrom(
                      backgroundColor: AppColors.buttonred,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  onPressed: () {},
                  icon: Icon(
                    Icons.call,
                    color: AppColors.whitehome,
                  ),
                  label: Text(
                    'Call Now',
                    style: TextStyle(
                        color: AppColors.whitehome,
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.vertical(6),
                width: SizeConfig.horizontal(44),
                child: TextButton.icon(
                  style: TextButton.styleFrom(
                      backgroundColor: AppColors.buttonblue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  onPressed: () {},
                  icon: Icon(
                    Icons.messenger_rounded,
                    color: AppColors.whitehome,
                  ),
                  label: Text(
                    'Send Message',
                    style: TextStyle(
                        color: AppColors.whitehome,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    ),
  );
}

///Widget Membuat scroll listview
Widget bottom(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
    child: Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: SizeConfig.horizontal(5.5)),
          child: Row(
            children: [
              Text(
                'Prevention',
                style: TextStyle(
                    color: AppColors.blackfirst,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
              )
            ],
          ),
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: SizedBox(
                height: SizeConfig.vertical(30),
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          'assets/images/1m.jpg',
                          width: 170,
                          height: 150,
                        ),
                        Container(
                          padding:
                              EdgeInsets.only(left: SizeConfig.horizontal(3.5)),
                          margin: EdgeInsets.only(
                              left: SizeConfig.horizontal(8),
                              top: SizeConfig.vertical(1)),
                          child: Text(
                            'Avoid Close\n Contact',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: AppColors.blackfirst,
                                fontSize: 15,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(right: SizeConfig.horizontal(2.5)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/images/batuk.jpg',
                            width: 170,
                            height: 150,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: SizeConfig.horizontal(3.5)),
                            margin: EdgeInsets.only(
                                left: SizeConfig.horizontal(8),
                                top: SizeConfig.vertical(1)),
                            child: Text(
                              'Avoid Close\n Contact',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: AppColors.blackfirst,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(right: SizeConfig.horizontal(1.5)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/images/cucitangan.jpg',
                            width: 170,
                            height: 150,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: SizeConfig.horizontal(3.5)),
                            margin: EdgeInsets.only(
                                left: SizeConfig.horizontal(8),
                                top: SizeConfig.vertical(1)),
                            child: Text(
                              'Avoid Close\n Contact',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: AppColors.blackfirst,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          'assets/images/demam.jpg',
                          width: 170,
                          height: 150,
                        ),
                        Container(
                          padding:
                              EdgeInsets.only(left: SizeConfig.horizontal(3.5)),
                          margin: EdgeInsets.only(
                              left: SizeConfig.horizontal(8),
                              top: SizeConfig.vertical(1)),
                          child: Text(
                            'Avoid Close\n Contact',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: AppColors.blackfirst,
                                fontSize: 15,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Container(
          width: SizeConfig.horizontal(90),
          height: SizeConfig.vertical(15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: <Color>[AppColors.purple, AppColors.whitehome]),
          ),
          child: Padding(
            padding: EdgeInsets.only(right: SizeConfig.horizontal(1.5)),
            child: ElevatedButton.icon(
              icon: Image.asset('assets/images/RS.png'),
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              label: RichText(
                text: TextSpan(
                    text: 'Do Your Own Test!\n',
                    style: TextStyle(
                        color: AppColors.whitehome,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    children: <TextSpan>[
                      TextSpan(
                          text:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.a\n',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 12,
                              fontWeight: FontWeight.w300)),
                      TextSpan(
                          text: 'Cras commodo posuere diam eu tristique.',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 12,
                              fontWeight: FontWeight.w300))
                    ]),
              ),
              onPressed: () {},
            ),
          ),
        ),
      ],
    ),
  );
}

/// Widget untuk UI Test Covid
Widget test(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(bottom: SizeConfig.vertical(3)),
    width: SizeConfig.horizontal(30),
    height: SizeConfig.vertical(10),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(20),
      gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: <Color>[AppColors.purple, AppColors.whitehome]),
    ),
  );
}

