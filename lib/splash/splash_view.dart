import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Utils/appcolors.dart';
import 'splash_controller.dart';

class SplashView extends GetView<SplashController>{
  const SplashView({Key? key}) : super(key: key); 
  @override 
  Widget build(BuildContext context){
    return MaterialApp(
      home: Image.asset('assets/images/logo.png',height: 20,width: 20,),
      color: AppColors.greenhome,
    );
  }
}