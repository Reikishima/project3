// ignore_for_file: depend_on_referenced_packages

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
 

class SplashController extends GetxController {
  final simpan = GetStorage();

  @override
  void onReady() {
    super.onReady();
    Future.delayed(const Duration(seconds: 10));
    Get.offAllNamed('/home');
  }
}
