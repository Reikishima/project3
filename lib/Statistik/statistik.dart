// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:project3/Utils/appcolors.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import '../Utils/sizeconfig.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:animated_button_bar/animated_button_bar.dart';

class Statistik extends StatefulWidget {
  const Statistik({super.key});

  @override
  State<Statistik> createState() => _StatistikState();
}

class _StatistikState extends State<Statistik> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return MaterialApp(
      home: Scaffold(
        body: Container(
          color: AppColors.bluehome,
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: SizeConfig.vertical(2.5),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: SizeConfig.screenWidth,
                      height: SizeConfig.vertical(59.9),
                      color: AppColors.bluehome,
                      child: ResponsiveRowColumn(
                        layout: ResponsiveRowColumnType.COLUMN,
                        children: <ResponsiveRowColumnItem>[
                          ResponsiveRowColumnItem(
                            child: row1(context),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: SizeConfig.vertical(0)),
                child: Container(
                    width: SizeConfig.screenWidth,
                    height: SizeConfig.vertical(30),
                    decoration: BoxDecoration(
                      color: AppColors.whitehome,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: SizeConfig.horizontal(25)),
                      child: Image.asset(
                        'assets/images/chart.png',
                        width: 70,
                        height: 70,
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Widget untuk UI pada container 1
Widget row1(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      top: SizeConfig.vertical(2.5),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.drag_handle_rounded,
                color: AppColors.whitehome,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.notifications_none,
                color: AppColors.whitehome,
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              padding: EdgeInsets.only(
                  left: SizeConfig.horizontal(4), top: SizeConfig.vertical(1)),
              child: Text(
                'Statistics',
                style: TextStyle(
                    color: AppColors.whitehome,
                    fontSize: 20,
                    fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.only(
              top: SizeConfig.vertical(1.5),
              left: SizeConfig.horizontal(4),
              right: SizeConfig.horizontal(4)),
          child: AnimatedButtonBar(
            radius: 30,
            backgroundColor: AppColors.purplebutton,
            foregroundColor: AppColors.whitehome,
            innerVerticalPadding: 7,
            children: [
              ButtonBarEntry(
                onTap: () {},
                child: Text(
                  'My Country',
                  style: TextStyle(
                      color: AppColors.bluehome,
                      fontSize: 16,
                      fontWeight: FontWeight.w800),
                ),
              ),
              ButtonBarEntry(
                onTap: () {},
                child: Text(
                  'Global',
                  style: TextStyle(
                      color: AppColors.bluehome,
                      fontSize: 16,
                      fontWeight: FontWeight.w800),
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            top: SizeConfig.vertical(1.5),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                'Total',
                style: TextStyle(
                    color: AppColors.whitehome, fontWeight: FontWeight.w400),
              ),
              Text(
                'Today',
                style: TextStyle(
                    color: AppColors.grey, fontWeight: FontWeight.w400),
              ),
              Text(
                'Yesterday',
                style: TextStyle(
                    color: AppColors.grey, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: SizeConfig.vertical(1.5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(
                width: SizeConfig.horizontal(45),
                height: SizeConfig.vertical(12),
                child: ElevatedButton(
                  style: TextButton.styleFrom(
                    backgroundColor: AppColors.orangebutton,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {},
                  child: Container(
                    margin: EdgeInsets.only(
                      right: SizeConfig.horizontal(17),
                      top: SizeConfig.vertical(2),
                    ),
                    padding: EdgeInsets.only(
                      bottom: SizeConfig.vertical(2),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Affected\n',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 14,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          '336,851',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.horizontal(45),
                height: SizeConfig.vertical(12),
                child: ElevatedButton(
                  style: TextButton.styleFrom(
                    backgroundColor: AppColors.buttonred,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {},
                  child: Container(
                    margin: EdgeInsets.only(
                        right: SizeConfig.horizontal(17),
                        top: SizeConfig.vertical(2)),
                    padding: EdgeInsets.only(
                      bottom: SizeConfig.vertical(2),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Death\n',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 14,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          '9,620',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 20,
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            left: SizeConfig.horizontal(1.5),
            top: SizeConfig.vertical(1.5),
            right: SizeConfig.horizontal(1.5),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(
                width: SizeConfig.horizontal(27),
                height: SizeConfig.vertical(15),
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: AppColors.greenbutton,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {},
                  child: Container(
                    margin: EdgeInsets.only(
                      top: SizeConfig.vertical(1),
                    ),
                    padding: EdgeInsets.only(
                      right: SizeConfig.horizontal(3),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Discovered\n',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                        Text(
                          '17,977',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.horizontal(27),
                height: SizeConfig.vertical(15),
                child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: AppColors.bluebutton,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    onPressed: () {},
                    child: Container(
                      margin: EdgeInsets.only(
                        top: SizeConfig.vertical(1),
                      ),
                      padding: EdgeInsets.only(
                        right: SizeConfig.horizontal(3),
                      ),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Active\n',
                              style: TextStyle(
                                  color: AppColors.whitehome,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              '301,251',
                              style: TextStyle(
                                  color: AppColors.whitehome,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500),
                            )
                          ]),
                    )),
              ),
              SizedBox(
                width: SizeConfig.horizontal(27),
                height: SizeConfig.vertical(15),
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: AppColors.greenhome,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {},
                  child: Container(
                    margin: EdgeInsets.only(top: SizeConfig.vertical(1)),
                    padding: EdgeInsets.only(
                      right: SizeConfig.horizontal(3),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Serious\n',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                        Text(
                          '8,702',
                          style: TextStyle(
                              color: AppColors.whitehome,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    ),
  );
}

/// Widget UI untuk diagram chart
Widget chart(BuildContext context) {
  return Scaffold(
    body: Center(
      child: SizedBox(
        child: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.vertical(50),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50)),
            color: AppColors.blackfirst,
          ),
          child: SfCartesianChart(
            plotAreaBorderColor: AppColors.blackfirst,
            plotAreaBackgroundColor: AppColors.whitehome,
            backgroundColor: AppColors.whitehome,

            // Initialize category axis
            primaryXAxis: CategoryAxis(),
            series: <LineSeries<SalesData, String>>[
              LineSeries<SalesData, String>(
                  // Bind data source
                  dataSource: <SalesData>[
                    SalesData('Jan', 35),
                    SalesData('Feb', 28),
                    SalesData('Mar', 34),
                    SalesData('Apr', 32),
                    SalesData('May', 40)
                  ],
                  xValueMapper: (SalesData sales, _) => sales.year,
                  yValueMapper: (SalesData sales, _) => sales.sales)
            ],
          ),
        ),
      ),
    ),
  );
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
// child: SizedBox(
//   height: SizeConfig.vertical(5),
//   child: ToggleButtons(
//     onPressed: (int index) {
//       setState(() {
//         // The button that is tapped is set to true, and the others to false.
//         for (int i = 0; i < _selectedFruits.length; i++) {
//           _selectedFruits[i] = i == index;
//         }
//       });
//     },
//     borderRadius: const BorderRadius.all(Radius.circular(20)),
//     selectedColor: AppColors.bluehome,
//     color: AppColors.blackfirst,
//     fillColor: AppColors.whitehome,
//     isSelected: _selectedFruits,
//     children:const  <Widget>[
//       Text('My Country',),
//       Text('Global',),
//     ],
//   ),
// ),
